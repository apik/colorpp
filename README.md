# README #

Feynman diagram color factor numerical evaluation software.

### Installation: ###

Install Eigen library [http://eigen.tuxfamily.org]
And specify path to it in Makefile

simply type `$ make `

### Usage: ###

Specify number of colors as first argument

`$./colorpp <NC>`

than using standard input stream specify color structure using:

* f(1,2,3) - for structure constants

* [T(1)T(2)T(3)] - for trace of generators

To run tests use
`source tests.sh`
